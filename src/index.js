// import { autocompleteOccupationName } from 'jobtech-taxonomy-autocomplete-occupation-name';

const input = document.querySelector("input");
const log = document.getElementById("values");

input.addEventListener("input", updateValue);

function updateValue(e) {
  log.textContent = jobtechTaxonomyAutocompleteOccupationName.autocompleteOccupationName(e.target.value).map(c => c.label + "\n").slice(0, 20);     
}